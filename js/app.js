const table = document.createElement('table');
document.body.append(table);
table.className = 'table';
const arrCell = [];
const arrNum = [];

class Field {
  constructor() {
    this.arrCell = arrCell;
    this.arrNum = arrNum;
  }

  buildField(num) {
    for (let i = 0; i < num; i++) {
      const cell = document.createElement('div');
      table.appendChild(cell);
      cell.className = 'cell';
      cell.style.backgroundColor = 'lightgray';
      this.arrCell.push(cell);
      this.arrNum.push(i);
    }
  }
}

class Cell {
  constructor() {
    this.arrNum = arrNum;
  }

  activeCell() {
    return this.arrNum.splice(Math.floor(Math.random() * this.arrNum.length), 1);
  }
}

class Game {
  constructor() {
    this.level = this.selectLevel();
    if (this.level === 'easy') {
      this.timeInterval = 1500;
    }
    else if (this.level === 'medium') {
      this.timeInterval = 1000;
    }
    else if (this.level === 'hard') {
      this.timeInterval = 500;
    }
    this.pointsUser = 0;
    this.pointsPc = 0;
    this.timer = null;
  }

  selectLevel() {
    let enterLevel;
    while (enterLevel !== 1 && enterLevel !== 2 && enterLevel !== 3) {
      enterLevel = +prompt('Enter one of the difficulty level. Easy: 1, Medium: 2, Hard: 3');
    }
    if (enterLevel === 1) return 'easy';
    else if (enterLevel === 2) return 'medium';
    else if (enterLevel === 3) return 'hard';
  }

  playerAction() {
    table.addEventListener('click', function (el) {
      if (el.target.style.backgroundImage === 'url("./img/activeIcon.png")') {
        const cell = el.target;
        cell.style.backgroundImage = 'url("./img/catchIcon.png")';
        clearTimeout(this.timer);
      }
    });
  }

  defineWinner() {
    if (this.pointsUser > this.pointsPc) {
      alert(`Congratulations! You've won the game! Score is ${this.pointsUser} : ${this.pointsPc}`);
    }
    else if (this.pointsUser < this.pointsPc) {
      alert(`You've lost the game! Score is ${this.pointsUser} : ${this.pointsPc}`);
    }
    alert('Good luck in new game!');
    location.reload();
  }

  startGame() {
    const goal = cell.activeCell();
    if (this.pointsUser >= arrCell.length / 2 || this.pointsPc >= arrCell.length / 2) {
      return this.defineWinner();
    }

    if (arrCell[goal].style.backgroundImage === '') {
      arrCell[goal].style.backgroundImage = 'url("./img/activeIcon.png")';
      arrNum.slice(arrNum.indexOf(goal), 1);
      this.playerAction();
      this.timer = setTimeout(() => {
        if (arrCell[goal].style.backgroundImage !== 'url("./img/catchIcon.png")') {
          arrCell[goal].style.backgroundImage = 'url("./img/loseIcon.png")';
          this.pointsPc++;
        } else if (arrCell[goal].style.backgroundImage === 'url("./img/catchIcon.png")') {
          this.pointsUser++;
        }
        this.timer = null;

        this.startGame();
      }, this.timeInterval);
    }
  }
}

const newField = new Field();
newField.buildField(100);
const cell = new Cell();
const newGame = new Game();
newGame.startGame();